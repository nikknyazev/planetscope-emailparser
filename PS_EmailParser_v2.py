# V2.
#
# Сортировать письма по времени (datetime.now() < 24 hour).
# Парсим письма (24 Hour) -> достаем нужные ссылки -> после парсинга играем с ссылками. Ссылки держим в словарях.
#
import imaplib
import email
import re
import os
import wget
import time
import configparser
import sys
import urllib
import traceback
import json

from datetime import datetime
from dateutil import parser

start_time = time.time()
datetime_now = datetime.now()

# Delete tmp files in download path
def delete_tmpFiles(load_path):
    for file in os.listdir(load_path):
        if file.endswith("tmp"):
            os.remove(os.path.join(load_path, file))
            print("Удален временный файл: {}".format(file))


# Bar progress for download visualization in PyCharm
def bar_progress(current, total, width=80):
  progress_message = "Downloading : %d%% [%d / %d] bytes" % (current / total * 100, current, total)
  sys.stdout.write("\r" + progress_message)
  sys.stdout.flush()

# Dictionary for downloading save links
def create_dict(dict, order, id_order, id_msg, link, file_name):
    intern_dict = {"id_order": id_order,
                   "id_msg": id_msg,
                   "link": link,
                   "file_name": file_name}
    dict[order] = intern_dict
    return dict

# Check email in json file
def check_save_email(message_id, id_order):
    with open("save_email.json", "r") as jsonFile:
        flag = 0    # No email
        data = json.load(jsonFile)
        for email in data:
            if data[email]["uid"] == message_id and data[email]["id_order"] == id_order:
                flag = 1
        return flag


# Add new email in json file
def add_new_email(order, id_order, file_name, message_id):
    with open("save_email.json", "r") as jsonFile:
        data = json.load(jsonFile)
        new_email = "email" + str(len(data) + 1)
        data[new_email] = {
            "order": order,
            "id_order": id_order,
            "file": file_name,
            "uid": message_id,
            "value": 1
            }
    with open("save_email.json", "w") as file:
        json.dump(data, file, indent=2)


################ CONFIG_FILE ###########################

configObj = configparser.ConfigParser()
configObj.read("settings.ini")

emailHost = configObj["Email"]["host"]
emailLogin = configObj["Email"]["login"]
emailPassword = configObj["Email"]["password"]

################ DOWNLOAD_PATH ##############################

# Preparing the download directory //
# Downloaded Files Folder Name
downloadFolder = "Download Link"
if not os.path.exists(os.path.join(os.getcwd(), downloadFolder)):
    os.mkdir(downloadFolder)
load_path = os.path.join(os.getcwd(), downloadFolder)
delete_tmpFiles(load_path)

################ IMAP SSL ##############################

with imaplib.IMAP4_SSL(host=emailHost, port=imaplib.IMAP4_SSL_PORT) as imap_ssl:
    print("Connection Object : {}".format(imap_ssl))

    ############### Login to Mailbox ######################
    print("Logging into mailbox...")
    resp_code, response = imap_ssl.login(emailLogin, emailPassword)

    print("Response Code : {}".format(resp_code))
    print("Response      : {}\n".format(response[0].decode()))

    ############### Set Mailbox #############
    resp_code, mail_count = imap_ssl.select(mailbox="Inbox", readonly=True)

    ############### Retrieve Mail IDs for given Directory #############
    resp_code, mails = imap_ssl.search(None, "ALL")
    print("Mail IDs : {}\n".format(mails[0].decode().split()))

    ############### Display All Messages for Planet Order Subject #############
    active_link = 0
    link_dict = {}
    for mail_id in mails[0].decode().split():
        # Get UID Message
        typ, message_id = imap_ssl.fetch(mail_id, '(BODY[HEADER.FIELDS (MESSAGE-ID)])')
        uid_message = email.message_from_bytes(message_id[0][1])

        # Get Text Message
        resp_code, mail_data = imap_ssl.fetch(mail_id, '(RFC822)')  ## Fetch mail data.
        message = email.message_from_bytes(mail_data[0][1])  ## Construct Message from mail data

        msg_date = parser.parse(message.get("Date"))
        timeStatus = datetime.timestamp(datetime_now) - datetime.timestamp(msg_date)  # Сколько прошло времени с момента получения письма (в сек)

        if ("Fwd: Planet Order" in message.get('Subject')) and (timeStatus < 86400):
            active_link += 1
            print("================== Start of Planet Order Mail [{}] ====================".format(mail_id))
            print("From        : {}".format(message.get("From")))
            print("To          : {}".format(message.get("To")))
            print("Date        : {}".format(message.get("Date")))
            print("Subject     : {}".format(message.get("Subject")))
            for part in message.walk():
                if part.get_content_type() == "text/plain":
                    msg = part.get_payload(None, True) # for encoded message
                    data_name = re.findall(b"Planet Order \*(\S+)\*", msg)[0]
                    load_link = re.findall(b"<(https://\S+)>\*", msg)[0]
                    file_name = re.findall(b"\*Name\*: (\S+.zip)", msg)[0]
                    id_order = re.findall(b"\*ID\*:\s(\S+)", msg)[0]
                    id_msg = uid_message.get("Message-ID")

                    data_name = str(data_name, 'utf-8')
                    load_link = str(load_link, 'utf-8')
                    file_name = str(file_name, 'utf-8')
                    id_order = str(id_order, 'utf-8')
                    print("Order       : {}".format(data_name))
                    print("ID order    : {}".format(id_order))
                    print("Link        : {}".format(load_link))
                    print("File Name   : {}".format(file_name))

                    link_dict = create_dict(link_dict, data_name, id_order, id_msg, load_link, file_name)

                    print("================== End of Planet Order Mail [{}] ====================\n".format(mail_id))

    if active_link == 0:
        print("No active link")
    ############# Close Selected Mailbox #######################
    print("Closing selected mailbox....\n")
    imap_ssl.close()

    ############################################################
    ########## Download Data From Save Links ###################
    ############################################################
    for email in link_dict:
        if check_save_email(link_dict[email]["id_msg"], link_dict[email]["id_order"]) == 0:
            try:
                tmp_file_path = os.path.join(load_path, link_dict[email]["file_name"] + ".tmp")
                download_file = wget.download(link_dict[email]["link"], out=os.path.join(load_path, tmp_file_path), bar=bar_progress)
                # Если загрузка успешна, переименовать tmp файл в zip архив
                os.rename(tmp_file_path, os.path.join(load_path, link_dict[email]["file_name"]))
                add_new_email(email, link_dict[email]["id_order"], link_dict[email]["file_name"], link_dict[email]["id_msg"])
                print("")
            except urllib.error.HTTPError:
                print("Status Code : 404 (Inactive link)")
            except Exception:
                print('\nОшибка:\n', traceback.format_exc())
        elif check_save_email(link_dict[email]["id_msg"], link_dict[email]["id_order"]) == 1:
            print("{} is already uploaded to the system".format(email))

print("Время работы программы: {} sec".format(str(time.time()-start_time)))