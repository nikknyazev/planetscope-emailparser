import imaplib
import email
import re
import os
import wget
import time
import configparser
import sys
import urllib
import traceback
import json
# import shutil

start_time = time.time()

# Delete tmp files in download path
def delete_tmpFiles(load_path):
    for file in os.listdir(load_path):
        if file.endswith("tmp"):
            os.remove(os.path.join(load_path, file))
            print("Удален временный файл: {}".format(file))


# Bar progress for download visualization in PyCharm
def bar_progress(current, total, width=80):
  progress_message = "Downloading : %d%% [%d / %d] bytes" % (current / total * 100, current, total)
  sys.stdout.write("\r" + progress_message)
  sys.stdout.flush()


# Check email in json file
def check_save_email(message_id):
    with open("save_email.json", "r") as jsonFile:
        flag = 0    # No email
        data = json.load(jsonFile)
        for email in data:
            if data[email]["uid"] == message_id:
                if data[email]["value"] == 1:
                    flag = 1    # Email in file, data was downloaded earlier
                # else:
                #     flag = 2    # Email in file, data not downloaded by mistake
        return flag


# Add new email in json file
def add_new_email(order, file_name, message_id):
    with open("save_email.json", "r") as jsonFile:
        data = json.load(jsonFile)
        new_email = "email" + str(len(data) + 1)
        data[new_email] = {
            "order": order,
            "file": file_name,
            "uid": message_id,
            "value": 1
            }
    with open("save_email.json", "w") as file:
        json.dump(data, file, indent=2)


################ CONFIG_FILE ###########################

configObj = configparser.ConfigParser()
configObj.read("settings.ini")

emailHost = configObj["Email"]["host"]
emailLogin = configObj["Email"]["login"]
emailPassword = configObj["Email"]["password"]

################ DOWNLOAD_PATH ##############################

# Preparing the download directory //
# Downloaded Files Folder Name
downloadFolder = "Download Link"
if not os.path.exists(os.path.join(os.getcwd(), downloadFolder)):
    os.mkdir(downloadFolder)
load_path = os.path.join(os.getcwd(), downloadFolder)
delete_tmpFiles(load_path)

################ IMAP SSL ##############################

with imaplib.IMAP4_SSL(host=emailHost, port=imaplib.IMAP4_SSL_PORT) as imap_ssl:
    print("Connection Object : {}".format(imap_ssl))

    ############### Login to Mailbox ######################
    print("Logging into mailbox...")
    resp_code, response = imap_ssl.login(emailLogin, emailPassword)

    print("Response Code : {}".format(resp_code))
    print("Response      : {}\n".format(response[0].decode()))

    ############### Set Mailbox #############
    resp_code, mail_count = imap_ssl.select(mailbox="Inbox", readonly=True)

    ############### Retrieve Mail IDs for given Directory #############
    resp_code, mails = imap_ssl.search(None, "ALL")
    print("Mail IDs : {}\n".format(mails[0].decode().split()))

    ############### Display All Messages for Planet Order Subject #############
    for mail_id in mails[0].decode().split():
        # Get UID Message
        typ, message_id = imap_ssl.fetch(mail_id, '(BODY[HEADER.FIELDS (MESSAGE-ID)])')
        uid_message = email.message_from_bytes(message_id[0][1])

        # Get Text Message
        resp_code, mail_data = imap_ssl.fetch(mail_id, '(RFC822)')  ## Fetch mail data.
        message = email.message_from_bytes(mail_data[0][1])  ## Construct Message from mail data
        if "Fwd: Planet Order" in message.get('Subject'):
            print("================== Start of Planet Order Mail [{}] ====================".format(mail_id))
            print("From        : {}".format(message.get("From")))
            print("To          : {}".format(message.get("To")))
            print("Date        : {}".format(message.get("Date")))
            print("Subject     : {}".format(message.get("Subject")))
            for part in message.walk():
                if part.get_content_type() == "text/plain":
                    msg = part.get_payload(None, True) # for encoded message
                    data_name = re.findall(b"Planet Order \*(\S+)\*", msg)[0]
                    load_link = re.findall(b"<(https://\S+)>\*", msg)[0]
                    file_name = re.findall(b"\*Name\*: (\S+.zip)", msg)[0]

                    data_name = str(data_name, 'utf-8')
                    load_link = str(load_link, 'utf-8')
                    file_name = str(file_name, 'utf-8')
                    print("Order       : {}".format(data_name))
                    print("Link        : {}".format(load_link))
                    print("File Name   : {}".format(file_name))

                    if check_save_email(uid_message.get("Message-ID")) == 0:
                        try:
                            # delete_tmpFiles(load_path)  # Надо ли перед каждым новым письмом удалять все тмп файлы в download
                            tmp_file_path = os.path.join(load_path, file_name + ".tmp")
                            download_file = wget.download(load_link, out=os.path.join(load_path, tmp_file_path), bar=bar_progress)
                            # Если загрузка успешна, переименовать tmp файл в zip архив
                            os.rename(tmp_file_path, os.path.join(load_path, file_name))    # Не дает сохранить файл с тем же названием, не добавляет лишнего в Json.
                            # shutil.move(tmp_file_path, os.path.join(load_path, file_name))  # Перезапись файла с тем же именем, добавление лишней записи в Json файл
                            add_new_email(data_name, file_name, uid_message.get("Message-ID"))
                            print("")
                        except urllib.error.HTTPError:
                            print("Status Code : 404 (Inactive link)")
                        except Exception:
                            print('\nОшибка:\n', traceback.format_exc())
                    elif check_save_email(uid_message.get("Message-ID")) == 1:
                        print("File is already uploaded to the system")

            print("================== End of Planet Order Mail [{}] ====================\n".format(mail_id))

    ############# Close Selected Mailbox #######################
    print("\nClosing selected mailbox....")
    imap_ssl.close()

print("Время работы программы: {} sec".format(str(time.time()-start_time)))